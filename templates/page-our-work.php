<?php
   /*
    Template Name: Our Work
   */
   ?>
<?php 
get_header(); 

define('CURR_ID', get_the_ID());

get_template_part('template-parts/our-work/banner');

get_template_part('template-parts/our-work/flexible-content');

get_template_part('template-parts/our-work/our-clients');

get_template_part('template-parts/contact-us');

get_footer();
?>
