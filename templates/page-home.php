<?php
   /*
    Template Name: Home
   */
   ?>
<?php 
get_header(); 

define('CURR_ID', get_the_ID());

get_template_part('template-parts/home/top-video');

get_template_part('template-parts/home/get-in-touch');

get_template_part('template-parts/home/informed');

get_template_part('template-parts/home/case-study');

get_template_part('template-parts/blog-posts');

get_template_part('template-parts/contact-us');

get_footer();
?>
