<?php
   /*
    Template Name: Contact Us
   */
   ?>
<?php 
get_header(); 

define('CURR_ID', get_the_ID());

get_template_part('template-parts/contact/banner-form');

get_template_part('template-parts/contact/map');

get_footer();
?>
