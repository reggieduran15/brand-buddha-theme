<?php
  /*
    Template Name: Privacy Policy
   */
?>
<?php get_header(); ?>
  <section class="pp-banner">
    <div class="title-wrapper semi-full-width">
      <!-- <span <?php //echo get_field('line_color', CURR_ID) ? 'style="background:' . get_field('line_color', CURR_ID) . ';"' : ''; ?>></span> -->
      <h1><?php echo get_the_title(); ?></h1>
    </div>
    <div class="container featured-img">
      <?php if(get_sub_field('overlay_background_color')) : ?>
        <div class="overlay" <?php echo get_sub_field('overlay_background_color') ? 'style="background:' . get_sub_field('overlay_background_color') . ';"' : ''; ?>></div>
      <?php endif; ?>
      <div class="featured-image">
        <?php 
        /* grab the url for the full size featured image */
        $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
        <?php if($featured_img_url) { ?>
          <div class="image-container" style="background-image:url(<?php echo $featured_img_url; ?>);"></div>
        <?php } else { ?>
          <div style="background-image: url(https://source.unsplash.com/user/erondu/2000x900);"></div>
        <?php } ?>
      </div>
    </div>
  </section>
  <section id="primary" class="content-area">
    <main id="main" class="site-main " role="main">
      <section>
        <?php if(have_posts()): ?>
          <?php while(have_posts()) : the_post(); ?>
            <div class="semi-full-width">
              <div class="content-details">
                <?php the_content(); ?>
              </div>
            </div>
          <?php endwhile; ?>
        <?php endif; ?>
      </section>
    </main><!-- .site-main -->
  </section><!-- .content-area -->

<?php get_footer(); ?>