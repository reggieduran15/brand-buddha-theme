<?php
   /*
    Template Name: What We Do
   */
   ?>
<?php 
get_header(); 

define('CURR_ID', get_the_ID());

get_template_part('template-parts/what-we-do/banner');

get_template_part('template-parts/what-we-do/image-text');

get_template_part('template-parts/what-we-do/about-us');

get_template_part('template-parts/what-we-do/team-member');

get_template_part('template-parts/contact-us');

get_footer();
?>
