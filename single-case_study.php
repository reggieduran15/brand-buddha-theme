<?php
/**
 * The template for displaying all case studies.
 */
?>
<?php
get_header();
define('CURR_ID', get_the_ID());

get_template_part('template-parts/single-case_study/banner');

get_template_part('template-parts/single-case_study/flexible-content');

get_template_part('template-parts/single-case_study/related-posts');

get_template_part('template-parts/contact-us');

get_footer();
?>