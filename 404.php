
<?php
define('CURR_ID', get_the_ID());

get_header();
?>
<section class="error404 full-width">
  <div class="container">
    <div class="row">
      <div class="xs-12 md-6 mx-auto">
        <div id="countUp">
          <div class="number">404</div>
          <div class="text">Page not found</div>
          <div class="text">This may not mean anything.</div>
          <div class="text">I'm probably working on something that has blown up.</div>
        </div>
      </div>
    </div>
  </div>            
</section>

<?php
// get_template_part('template-parts/contact-us');

get_footer(); ?>