jQuery(function($) {

  $(".filter-item > a, .select-items div").click(function(e) {
    e.preventDefault();
    // AJAX
    var data = {
      action: "blogfilter",
      query: blog_loadmore_params.posts,
      category: $(this).attr('data-category'),
      page: parseInt(blog_loadmore_params.current_page) + 1
    };
    if ($("#blog_listings").length) {

      $.ajax({
        url: blog_loadmore_params.ajaxurl,
        data: data,
        type: "POST",

        success: function(response) {
          if (response.success) {
            var html = "";
            $(response.data.post).each(function(i, v) {
              html += '<div class="blog-post">';
              html += '<div class="blog-post-container">';
              html += '<div class="content"><time datetime="' + v.datetime + '" itemprop="datePublished">' + v.date + "</time>";
              html += '<h3 class="entry-title"><a href="' + v.permalink + '" title="' + v.post_title + '">' + v.post_title + "</a></h3>";
              html += '<div class="read-more"><a href="' + v.permalink + '" class="read-more">Read Article</a><span class="overlay" style="background:' + v.line_color +';"></span>';
              html += "</div></div>";
              html += '<div class="featured-image">';
              html += '<a href="' + v.permalink + '" rel="lightbox"></a>';
              html += '<div class="image-url" style="background-image:url(' + v.post_featured_image + ');"></div></div>';
              html += '</div></div>';

              if (v.post_count <= 4) {
                $("#more-stories").hide();
              } else {
                $("#more-stories").show();
              }
            });
            $("#blog_listings").html(html);
          } else {
            // do nothing
          }
          blog_loadmore_params.current_page++;
        }
      });
    }
  });
});
