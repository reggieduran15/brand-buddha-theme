jQuery(function($) {
  var canBeLoaded = true;
  $("#more-people").click(function() {
    // AJAX
    var data = {
      'action': 'teamloadmore',
      'query': team_loadmore_params.posts,
      'page' : parseInt(team_loadmore_params.current_page)+1
    };
    if($('#team-listings').length){
      $.ajax({
        url : team_loadmore_params.ajaxurl,
        data: data,
        type:'POST',
        beforeSend: function( xhr ){
          canBeLoaded = false; 
        },
        success:function(response){
          if(response.success){
            var html = '';
            $(response.data.post).each(function(i, v){     
                html += '<div class="tm-wrapper">';
                html += '<img class="bnw-avatar" src="'+v.bnw+'" alt="'+v.post_title+'">';
                html += '<img class="colored-avatar" src="'+v.colored+'" alt="'+v.post_title+'">';
                html += '<div class="info-wrapper">';
                html += '<span class="name">'+v.post_title+'</span>';
                html += '<span class="pos">'+v.pos+'</span>';
                html += '</div>';
                html += '</div>';
            });
            $('#team-listings').append(html);
            canBeLoaded = true; 
          } else {
            // do nothing
          }             
          team_loadmore_params.current_page++;
          $('.more-tm-btn').hide();
          $('#team-listings').css('padding','0 0 50px 0');
        }			
      });
    }
  });
});