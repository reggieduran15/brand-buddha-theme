if (jQuery) { $ = jQuery; }
if ($) { jQuery = $; }

var canBeLoaded = true;

$(document).ready(function() {
	$('.work_loadwork').click(function(e){
 		e.preventDefault();
		var button = $(this);
		var data = {
			'action': 'loadwork',
			'query': work_loadwork_params.posts,
			'page' : work_loadwork_params.current_page
		};
 
		$.ajax({ // you can also use $.post here
			url : work_loadwork_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {				
				button.text('Loading...');
			},
			success : function( response ){
				if( response.success ) {
					if(response.data.post.length > 0){
						var html = '';
						$(response.data.post).each(function(i, v){

							html += '<div class="item--column item--column-3 show" data-cat="'+v.post_term+'">'
							html += '<a href="'+v.permalink+'">'
							html += '<div class="image--box">'
							html += '<img src="'+v.post_image+'" width="411" height="auto" alt="" title="" class="work-thumb">'
							html += '</div>'

							html += '<div class="desc--overlay">'
							html += '<div class="desc--content">'
							html += '<img src="<?php echo get_template_directory_uri(); ?>/src/images/icon-search.png" width="44" height="auto" alt="" title="">'
							html += '<h5>'+v.post_title+'</h5>'
							html += '<p>'+v.post_excerpt+'</p>'
							html += '</div>'
							html += '</div>'
							html += '</a>'
							html += '</div>'

						});
						
						button.text( 'Load More' ).prev().before(data);

						$('#list--work').append(html);
						button.show();

						if (response.data.post.length < 9) {
							button.hide();
						}

					} else {
                        button.hide();
                    }

                    canBeLoaded = true;

				} else {
					// do nothing
				}

				work_loadwork_params.current_page++;
			}
		});
	});
});