jQuery(function($) {
  var canBeLoaded = true;

  $("#insights_loadmore").click(function() {
    // AJAX
    var button = $(this),
      data = {
        action: "blogloadmore",
        query: blog_loadmore_params.posts,
        page: parseInt(blog_loadmore_params.current_page) + 1
      };

    if (canBeLoaded == true && $("#blog_listings").length) {
      $.ajax({
        url: blog_loadmore_params.ajaxurl,
        data: data,
        type: "POST",
		
        beforeSend: function(xhr) {
          canBeLoaded = false;
        },

        success: function(response) {
          if (response.success) {
            var html = "";
            $(response.data.post).each(function(i, v) {
              html += '<div class="blog-post">';
              html += '<div class="blog-post-container">';
              html += '<div class="content"><time datetime="' + v.datetime + '" itemprop="datePublished">' + v.date + "</time>";
              html += '<h3 class="entry-title"><a href="' + v.permalink + '" title="' + v.post_title + '">' + v.post_title + "</a></h3>";
              html += '<div class="read-more"><a href="' + v.permalink + '" class="read-more">Read Article</a><span class="overlay" style="background:' + v.line_color +';"></span>';
              html += "</div></div>";
              html += '<div class="featured-image">';
              html += '<a href="' + v.permalink + '" rel="lightbox"></a>';
              html += '<div class="image-url" style="background-image:url(' + v.post_featured_image + ');"></div></div>';
              html += '</div></div>';
            });

            $("#blog_listings").append(html);

            canBeLoaded = true;
          } else {
            // do nothing
          }
          blog_loadmore_params.current_page++;

          button.parent().hide();
        }
      });
    }
  });
});
