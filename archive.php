
<?php
define('CURR_ID', get_option( 'page_for_posts' ));
get_header();

$categoryPost = get_queried_object();
$loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => 5, 'cat' => $categoryPost->term_id, 'post_status' => 'publish', 'order' => 'date' ) );
$inc = 1; 

if ( $loop->have_posts() ) : ?>
  <section class="story-list">
    <div id="story_listings">
      <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
      <?php if ( $inc==1 ) {
        $inc++; ?> 
        <div class="blog-post first-post">
          <div class="container blog-post-container semi-full-width">
            <div class="featured-image">
              <?php 
              /* grab the url for the full size featured image */
              $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
              echo '<a href="'. get_permalink() .'" rel="lightbox"></a>'; ?>
              <div class="image-container" style="background-image:url(<?php echo $featured_img_url; ?>);"></div>
            </div>
            <div class="content-details">
              <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h3>' ); ?>
              <span class="btn-wrapper button-hover" style="background: #890B6A;">
                <a class="read-full-art" href="<?php echo get_permalink(); ?>" class="read-more">Read Article</a>
                <span></span>
              </span>
            </div>
          </div>
        </div>
      <?php } endwhile; ?>
      <div class="container blog-post">
        <div class="blog-post-list">
        <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
          <div class="blog-post-container">
            <div class="container">
              <div class="content-details">
                <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h3>' ); ?>
                <div class="read-more">
                  <a href="<?php echo get_permalink(); ?>">Read Article</a>
                  <span class="overlay" <?php echo get_field('line_color', get_the_ID()) ? 'style="background:' . get_field('line_color', get_the_ID()) . ';"' : 'style="background:#FFCC28;"'; ?>></span>
                </div>
              </div>
              <div class="featured-image">
                <?php 
                /* grab the url for the full size featured image */
                $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
                echo '<a href="'. get_permalink() .'" rel="lightbox"></a>'; ?>
                <div class="image-container" style="background-image:url(<?php echo $featured_img_url; ?>);"></div>
              </div>
            </div>
          </div>
        <?php endwhile; ?>

        <!-- <div class="story-loadmore"> 
          <a id="story_loadmore" class="main-button btn loadmore">- Load more -</a>
        </div> -->
      </div>
    </div>
  </section>
<?php endif; 
get_footer(); ?>