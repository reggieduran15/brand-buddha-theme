<?php 
$currentID = get_the_ID();
$my_query = new WP_Query( array( 'showposts' => '3', 'post__not_in' => array($currentID)));
if($my_query->have_posts()) : ?>
  <div class="recent-post-section">
    <div class="title-container width-of">
      <span class="line" style="background: #ffcc05;"></span>
      <h3>Related Post</h3>
    </div>
    <div class="flex-column width-of">
      <?php while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
        <div class="custom-post">
          <div class="custom-post-container">
            <div class="content-overlay"></div>
            <div class="content">
              <span class="line" style="background: <?php echo get_field('line_color', get_the_ID()) ? get_field('line_color', get_the_ID()) : '#ffcc05' ?>;"></span>
              <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h3>' ); ?>
              <div class="button-hover">
                <a class="button" href="<?php echo get_permalink(); ?>">View Post</a>
                <span></span>
              </div>
            </div>

            <?php 
              /* grab the url for the full size featured image */
              $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
        
              /* link thumbnail to full size image for use with lightbox*/
              echo '<a href="'. get_permalink() .'" rel="lightbox"></a>'; 
            ?>
            <div class="image-url" style="background-image: url(<?php echo $featured_img_url; ?>"></div>
          </div>
        </div>
      <?php endwhile; ?>
    </div>
  <?php endif; ?>
  <?php wp_reset_postdata(); ?>
</div>