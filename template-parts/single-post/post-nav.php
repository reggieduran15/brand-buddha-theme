<div class="post-nav hide-desktop">
<?php
  $next_post_url = get_permalink( get_adjacent_post(false,'',false)->ID );
  $previous_post_url = get_permalink( get_adjacent_post(false,'',true)->ID );
?>
  <p class="prev"><img src="<?php echo get_template_directory_uri(); ?>/src/images/more-btn.png" alt=""><a href="<?php echo $previous_post_url; ?>">Previous</a></p>
  <p class="next"><img src="<?php echo get_template_directory_uri(); ?>/src/images/more-btn.png" alt=""><a href="<?php echo $next_post_url; ?>">Next</a></p>
</div>