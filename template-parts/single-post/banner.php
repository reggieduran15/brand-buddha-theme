<section class="sp-banner">
  <div class="title-wrapper">
    <span <?php echo get_field('line_color', CURR_ID) ? 'style="background:' . get_field('line_color', CURR_ID) . ';"' : ''; ?>></span>
    <h1><?php echo get_the_title(); ?></h1>
  </div>
  <div class="container featured-img">
    <?php if(get_sub_field('overlay_background_color')) : ?>
      <div class="overlay" <?php echo get_sub_field('overlay_background_color') ? 'style="background:' . get_sub_field('overlay_background_color') . ';"' : ''; ?>></div>
    <?php endif; ?>
    <div style="background-image: url(https://source.unsplash.com/user/erondu/2000x900);"></div>
  </div>
</section>