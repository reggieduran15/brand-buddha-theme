
<?php
//query arguments
$args = array(
  'post_type' => 'case_study',
  'post_status' => 'publish',
  'posts_per_page' => 3,
  'orderby' => 'DATE',
  'post__not_in' => array ($post->ID),
);
//the query
$relatedPosts = new WP_Query( $args );
//loop through query
if($relatedPosts->have_posts()) :
  ?>
  <div class="related-post-container">
    <div class="title-container width-of">
      <span class="line" style="background: #ffcc05;"></span>
      <h3>More Case Studies</h3>
    </div>
    <div class="flex-column width-of">
      <?php
      while($relatedPosts->have_posts()) : $relatedPosts->the_post();
        ?>
          <div class="custom-post">
            <div class="custom-post-container">
              <div class="content-overlay"></div>
              <div class="content">
                <span class="line" style="background: <?php echo get_field('line_color', get_the_ID()) ? get_field('line_color', get_the_ID()) : '#ffcc05' ?>;"></span>
                <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h3>' ); ?>
                <div class="entry-content">
                  <?php if (has_excerpt()) {
                    $excerpt = wp_strip_all_tags(get_the_excerpt());
                  } ?>
                  <?php 
                    //the_content(); 
                    echo $excerpt;
                  ?>
                </div>
                <div class="button-hover">
                  <a class="button" href="<?php echo get_permalink(); ?>">View Case Study</a>
                  <span></span>
                </div>
              </div>

              <?php 
                /* grab the url for the full size featured image */
                $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
          
                /* link thumbnail to full size image for use with lightbox*/
                echo '<a href="'. get_permalink() .'" rel="lightbox"></a>'; 
              ?>
              <div class="image-url" style="background-image: url(<?php echo $featured_img_url; ?>"></div>
            </div>
          </div>
        <?php
      endwhile;
      ?> 
    </div>
    <div id="more-work" class="more-work">
      <div class="button-hover white-bg">
        <a class="button" href="<?php echo get_site_url(); ?>/our-work">View More Work</a>
        <span></span>
      </div>
      </div>
    </div>

  </div>
  <?php
endif;

//restore original post data
wp_reset_postdata();
?>