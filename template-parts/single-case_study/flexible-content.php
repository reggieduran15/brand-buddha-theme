<section id="cs-flexible-content" class="cs-flexible-content">
  <div class="lotus-container show-dktp">
    <img class="lot1" src="<?php echo get_template_directory_uri();?>/src/images/lutos-1.png" alt="">
    <img class="lot2" src="<?php echo get_template_directory_uri();?>/src/images/lutos-2.png" alt="">
    <img class="lot3" src="<?php echo get_template_directory_uri();?>/src/images/lutos-3.png" alt="">
    <img class="lot4" src="<?php echo get_template_directory_uri();?>/src/images/lutos-4.png" alt="">
  </div>
  <!-- <img class="lotus-parallax show-dktp" src="<?php //echo get_template_directory_uri();?>/src/images/Lotus1.png" alt=""> -->
  <img class="show-mbl" src="<?php echo get_template_directory_uri();?>/src/images/Lotus-mobile.png" alt="">
  <?php

  // Check value exists.
  if( have_rows('content') ):

    // Loop through rows.
    while ( have_rows('content') ) : the_row();

      // Case: Right Column Text layout.
      if( get_row_layout() == 'right_column_text' ):
        // Do something...
        ?>
        <div class="container right_column_text" <?php echo get_sub_field('background_color') ? 'style="background-color:' . get_sub_field('background_color') . ';"' : ''; ?>>
          <div class="flex-column width-of">
            <div class="left"></div>
            <div class="right animated hiding" data-animation="fadeInUp" data-delay="200">
              <?php
                $text = get_sub_field('content');
                echo $text;
               ?>
            </div>
          </div>
        </div>
        <?php

      // Case: Upload Video layout.
      elseif( get_row_layout() == 'upload_video' ): 
        // Do something...
        ?>
        <div class="container upload_video animated hiding <?= get_sub_field('disable_full_width_in_mobile') == 'Yes' ? 'not-fullwidth' : ''; ?>" data-animation="fadeInUp" data-delay="200">
          <div class="width-of">
            <?php if(get_sub_field('enable_overlay_background') == 'Yes') : ?>
              <div class="overlay" <?php echo get_sub_field('overlay_background_color') ? 'style="background:' . get_sub_field('overlay_background_color') . ';"' : ''; ?>></div>
            <?php endif; ?>
            <video class="show-dktp" poster="<?php echo get_sub_field('upload_image_thumbnail')['url']; ?>"  autoplay muted playsinline>
              <?php echo get_sub_field('upload_video') ? ' <source src="' . get_sub_field('upload_video')['url'] . '" type="video/mp4">' : ''; ?>
            </video>
            <video class="show-mbl" poster="<?php echo get_sub_field('upload_image_thumbnail')['url']; ?>"  autoplay muted playsinline>
              <?php echo get_sub_field('upload_video') ? ' <source src="' . get_sub_field('upload_video')['url'] . '" type="video/mp4">' : ''; ?>
            </video>
            <?php
              if (get_sub_field('play_button_caption')) :
                ?>
                <button type="button" id="play_button">
                  <span style="border-color:<?php echo get_sub_field('line_color');?>">
                    <?php echo get_sub_field('play_button_caption'); ?>
                  </span>
                  <?php echo get_sub_field('upload_play_button_icon') ? '<img src="' . get_sub_field('upload_play_button_icon')['url'] . '" alt="">' : ''; ?>
                </button>
                <?php
              endif;
            ?>
          </div>
        </div>
        <?php

      // Case: Two Column Text layout.
      elseif( get_row_layout() == 'two_column_text' ): 
        // Do something...
        ?>
        <div class="container two_column_text animated hiding" data-animation="fadeInUp" data-delay="200">
          <div class="flex-column width-of" <?php echo get_sub_field('header_color') ? 'style="color:' . get_sub_field('header_color') . ';"' : ''; ?>>
            <div class="left">
              <?php
                $leftText = get_sub_field('left_column_content');
                echo $leftText;
              ?>
            </div>
              <div class="right">
                <div class="text-wrapper" id="num-animate">
                  <?php
                    $rightText = get_sub_field('right_column_content');
                    echo $rightText;
                  ?>
                </div>
              </div>
          </div>
        </div>
        <?php

      // Case: Two Column Image layout.
      elseif( get_row_layout() == 'two_column_image' ):
        // Do something...
        ?>
        <div class="container two_column_image <?php echo get_sub_field('enable_tile_design') ? 'tile-style': ''; ?> animated hiding" data-animation="fadeInUp" data-delay="200">
          <div class="flex-column width-of">
            <div class="left">
              <?php if(get_sub_field('left_image')): ?>
                <img src="<?php echo get_sub_field('left_image')['url']; ?>" alt="" />
              <?php endif; ?>
            </div>
            <div class="right">
              <?php if(get_sub_field('right_image')): ?>
                <img src="<?php echo get_sub_field('right_image')['url']; ?>" alt="" />
              <?php endif; ?>
            </div>
          </div>
        </div>
        <?php

      // Case: Image Only layout.
      elseif( get_row_layout() == 'image_only' ): 
        // Do something...
        ?>
        <div class="container image_only <?php echo get_sub_field('full_with_in_mobile') == 'Yes' ? 'fw-mobile' : ''; ?> animated hiding" data-animation="fadeInUp" data-delay="200">
          <div class="width-of">
            <?php echo get_sub_field('upload_image') ? '<img src="' .  get_sub_field('upload_image')['url'] . '" alt="" />' : ''; ?>
          </div>
        </div>
        <?php

      // Case: Upload Video(Smaller Siza) layout.
      elseif( get_row_layout() == 'upload_video_s' ): 
        // Do something...
        ?>
        <div class="container upload_video_s animated hiding" data-animation="fadeInUp" data-delay="200">
          <div class="width-of">
            <video class="show-dktp" loop muted poster="<?php echo get_sub_field('upload_image_thumbnail')['url']; ?>" playsinline autoplay>
              <?php echo get_sub_field('upload_video') ? ' <source src="' . get_sub_field('upload_video')['url'] . '" type="video/mp4">' : ''; ?>
            </video>
            <video class="show-mbl" loop muted poster="<?php echo get_sub_field('upload_image_thumbnail')['url']; ?>" playsinline autoplay>
              <?php echo get_sub_field('upload_video') ? ' <source src="' . get_sub_field('upload_video')['url'] . '" type="video/mp4">' : ''; ?>
            </video>
          </div>
        </div>
        <?php
        
      endif;

    // End loop.
    endwhile;

  // No value.
  else :
      // Do something...
  endif;
  ?>
</section>