<section class="cs-single-banner">
  <div class="title-wrapper">
    <span <?php echo get_field('line_color', CURR_ID) ? 'style="background:' . get_field('line_color', CURR_ID) . ';"' : ''; ?>></span>
    <h1><?php echo get_the_title(); ?></h1>
  </div>
  <div class="container">
    <?php 
      // Get the Video Fields
      $videoMp4 =  get_field('video_banner', CURR_ID); // MP4 Field Name
      $mobileVideoMp4 =  get_field('mobile_video_banner'); // MP4 Field Name
    ?>
    <?php if(get_field('overlay_bg', CURR_ID)) : ?>
      <div class="overlay" <?php echo get_field('overlay_bg', CURR_ID) ? 'style="background:' . get_field('overlay_bg', CURR_ID) . ';"' : ''; ?>></div>
    <?php endif; ?>
    <video class="show-dktp" loop muted playsinline autoplay>
      <source src="<?php echo $videoMp4['url']; ?>" type="video/mp4">
    </video>
    <video class="show-mbl" loop muted playsinline autoplay>
      <source src="<?php echo $mobileVideoMp4['url']; ?>" type="video/mp4">
    </video>
    
    <?php if(!$videoMp4):?>
      <?php if(get_field('overlay_bg', CURR_ID)) : ?>
        <div class="overlay" <?php echo get_field('overlay_bg', CURR_ID) ? 'style="background:' . get_field('overlay_bg', CURR_ID) . ';"' : ''; ?>></div>
      <?php endif; ?>
      <?= get_field('image_banner', CURR_ID) ? '<div class="background-image" style="background:url(' . get_field('image_banner', CURR_ID)['url'] . ');"></div>' : '';?>
    <?php endif; ?>
  </div>
</section>