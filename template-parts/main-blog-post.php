
<?php $blog_ID = get_option( 'page_for_posts' ); ?>
<section class="cat-list" id="cat-list">
  <ul id="category-list">
    <li id="All">All</li>
    <?php
      $categories = get_categories('post_type=case_study&orderby=term_order&exclude=1'); 
      foreach ($categories as $category) :
      echo '<li id="'.$category->name.'"';
      echo '>'.$category->name.'</li>';
      endforeach; 
    ?>
  </ul>
</section>

<section class="main-case-study">
  <div class="semi-full-width" id="work-listings" data-filter="<?php echo (isset($_GET['newscat'])) ? $_GET['newscat'] : false; ?>">

    <?php // let the queries begin 
    // $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;  
    $newslist = new WP_Query( array(
      'post_type' => 'case_study', 
      'posts_per_page' => 5,
      'post_status' => 'publish',
      'orderby' => 'DATE',
      // 'paged' => $paged
    ) ); 
    
    if ($newslist->have_posts()) :
    
      while ( $newslist->have_posts() ) : $newslist->the_post(); 
      ?>

        <div class="custom-post">

        <div class="custom-post-container">

          <div class="featured-image">

            <div class="featured-image-overlay"><p>View project</p></div>
            <?php 

            /* grab the url for the full size featured image */
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full'); 

            /* link thumbnail to full size image for use with lightbox*/
            echo '<a href="'. get_permalink() .'" rel="lightbox"></a>'; 

            ?>
            <div class="image-url" style="background-image: url(<?php echo $featured_img_url; ?>"></div>
            
          </div>
          
          <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h3>' ); ?>

          <div class="entry-content">

            <?php 
              echo has_excerpt() ? wp_strip_all_tags(get_the_excerpt()) : '';
            ?>

          </div>

        </div>

        </div>
    
      <?php endwhile; 
      
      else : 
        
        echo 'There are no news items in that category.'; 
   
      endif; 
    ?>  

  </div>

  <?php if( get_field('button_text', $blog_ID ) && $newslist->post_count >= 6 ) : ?>
    <a class="main-button btn" id="case_study_loadmore"><?php echo get_field('button_text', $blog_ID ); ?></a>
  <?php endif; ?>

</section>
