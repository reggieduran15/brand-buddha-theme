<?php 
  if ( class_exists( 'acf' ) ) {
    if ( get_field( 'logo', 'option' ) ) {
      $logo = get_field( 'logo', 'option' );
    }
    if ( get_field( 'sticky_logo', 'option' ) ) {
      $stickyLogo = get_field( 'sticky_logo', 'option' );
    }
    if ( get_field( 'mobile_logo', 'option' ) ) {
      $mobileLogo = get_field( 'mobile_logo', 'option' );
    }
    if ( get_field( 'facebook', 'option' ) ) {
      $facebook = get_field( 'facebook', 'option' );
    }
    if ( get_field( 'twitter', 'option' ) ) {
      $twitter = get_field( 'twitter', 'option' );
    }
    if ( get_field( 'instagram', 'option' ) ) {
      $instagram = get_field( 'instagram', 'option' );
    }
    if ( get_field( 'linkedin', 'option' ) ) {
      $linkedin = get_field( 'linkedin', 'option' );
    }
  }
?>

<section class="sticky-header">
  <div class="logo-menu-wrapper">
    <div class="logo">
      <a href="/" class="sticky-logo show-dktp"></a>
      <img class="sticky-logo show-dktp" src="<?php echo esc_url($stickyLogo['url']); ?>">
      <a href="/" class="logo show-dktp main"></a>
      <img class="show-dktp main" src="<?php echo esc_url($logo['url']) ?>">
      <a href="/" class="logo show-mbl"></a>
      <img class="show-mbl" src="<?php echo esc_url($mobileLogo['url']) ?>">
    </div>

    <div class="nav-desktop show-dktp">
      <?php 
        wp_nav_menu( array(
          'menu' => 'Main Menu',
          'menu_class' => 'header-menu',
        ) );
      ?>
    </div>
  </div>

  <div class="menu-hamburger">
    <span></span>
  </div>

  <div class="nav-mobile show-mbl">
    <?php 
      wp_nav_menu( array(
        'menu' => 'Main Menu',
        'menu_class' => 'header-menu',
    ) );
    ?>
    <div class="social-icons">
      <ul>
        <li class="menu-icon-fb"><a href="#"></a><img src="<?php echo esc_url($facebook['url']); ?>" alt=""></li>
        <li class="menu-icon-tw"><a href="#"></a><img src="<?php echo esc_url($twitter['url']); ?>" alt=""></li>
        <li class="menu-icon-ig"><a href="#"></a><img src="<?php echo esc_url($instagram['url']); ?>" alt=""></li>
        <li class="menu-icon-li"><a href="#"></a><img src="<?php echo esc_url($linkedin['url']); ?>" alt=""></li>
      </ul>
    </div>
    <a href="/contact-us" class="contact-us-btn show-mbl">Contact us</a>
  </div>
  <div class="button-hover white-bg">
    <a href="/contact-us" class="contact-us-btn show-dktp">Contact us</a>
  </div>
</section>