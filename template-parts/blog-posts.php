<?php $term = get_queried_object(); ?>
<?php if ( is_blog() ) : ?>
  <section class="cat-list show-dktp" id="cat-list">
    <ul id="category-list">
      <li class="filter-item">
        <a href="<?php echo get_site_url(); ?>/blog">All</a>
        <span <?= get_field('border_line_color', $term) ? 'style="background: ' . get_field('border_line_color', $term) . '"' : ''; ?>></span>
      </li>
      <?php
        $args = array(
          'post_type' => 'case_study',
          'orderby' => 'term_order',
          'exclude'=> array(1)
        );
        $categories = get_categories($args); 
        foreach ($categories as $category) : 
          ?>
          <li class="filter-item">
            <a data-category="<?= $category->term_id; ?>" href="<?= get_category_link($category->term_id); ?>"><?= $category->name ?></a>
            <span></span>
          </li>
        <?php endforeach;
      ?>  
    </ul>
  </section>
  <section class="cat-list show-mbl" id="cat-list">
    <div class="select-wrapper">
      <form action="" method="GET" id="newslist">
        <select name="category" id="category thedropdown" class="btn" onchange="submit();">
          <option value="" <?php echo (isset($_GET['category']) == '') ? ' selected="selected"' : ''; ?>>All Stories</option>
          <?php 
            $categories = get_categories('post_type=case_study&orderby=term_order&exclude=1'); 
            foreach ($categories as $category) :
              $categories = get_categories('post_type=case_study&orderby=term_order&exclude=1'); 
              echo '<option data-color="'.get_field('border_line_color',$category->term_id).'" id="'.$category->name.'" value="'.$category->term_id.'"';
              echo (isset($_GET['category']) &&  $_GET['category'] == ''.$category->term_id.'') ? ' selected="selected"' : '';
              echo '>'.$category->name.'</option>';
            endforeach; 
          ?>
        </select>
        <img src="<?php echo get_template_directory_uri();?>/src/images/more-btn.png" alt="">
      </form>
    </div>
  </section> 
<?php endif; ?>
<section class="our-blog">
  <div id="blog_listings">
  <?php if ( get_field('title_bp', CURR_ID) ) :  ?>
    <div class="title semi-full-width"><?php echo get_field('title_bp', CURR_ID); ?></div>
  <?php endif; ?>
  <?php // let the queries begin 
    // $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;  
    if( !isset($_GET['category']) || '' == $_GET['category']) {
        $loop = new WP_Query( array(
          'post_type' => 'post', 
          'posts_per_page' => is_blog() ? 5 : 3,
          'post_status' => 'publish',
          'orderby' => 'DATE',
          // 'paged' => $paged
        ) ); 
    } else { //if select value exists (and isn't 'show all'), the query that compares $_GET value and taxonomy term (name)
      $category = $_GET['category']; //get sort value
      $loop = new WP_Query( array(
        'post_type' => 'post', 
        'posts_per_page' => is_blog() ? 5 : 3,
        'post_status' => 'publish',
        'orderby' => 'DATE',
        // 'paged' => $paged,
        'cat' => intval($category ),
      ));
  } ?>
  <?php if ( $loop->have_posts() ) : ?>
    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
      <div class="blog-post">
        <div class="blog-post-container">
          <div class="content">
            <time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished"><?php echo get_the_date('n.j.Y'); ?></time>
            <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h3>' ); ?>
            <div class="read-more">
              <a href="<?php echo get_permalink(); ?>">Read Article</a>
              <span class="overlay" <?php echo get_field('line_color', get_the_ID()) ? 'style="background:' . get_field('line_color', get_the_ID()) . ';"' : 'style="background:#FFCC28;"'; ?>></span>
            </div>
          </div>
          <div class="featured-image">
            <?php 
            /* grab the url for the full size featured image */
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
            /* link thumbnail to full size image for use with lightbox*/
            echo '<a href="'. get_permalink() .'" rel="lightbox"></a>'; ?>
            <div class="image-url" style="background-image: url(<?php echo $featured_img_url; ?>"></div>
          </div>
        </div>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>
  </div>
  <?php if ( get_field('button_text_bp', CURR_ID) ) :  ?>
    <div class="semi-full-width learn-more button-hover white-bg"> 
      <a href="<?php echo get_field('button_link_bp', CURR_ID) ?  get_field('button_link_bp', CURR_ID) : '#'; ?>" class="button"><?php echo get_field('button_text_bp', CURR_ID); ?></a>
      <span></span>
    </div>
  <?php endif; ?>
</section>
<section id="more-stories">
  <?php if ( is_blog() && $loop->post_count >= 5 ) : ?>
    <div class="semi-full-width more-stories"> 
      <a id="insights_loadmore" class="main-button btn">Load More Stories</a>
      <img src="<?php echo get_template_directory_uri();?>/src/images/more-btn.png" alt="">
    </div>
  <?php endif;?>
</section>
<?php wp_reset_postdata(); ?>