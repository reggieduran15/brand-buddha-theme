<?php 
  // Get the Video Fields
?>

<section class="banner-form">
  <!-- <div class="overlay"></div> -->
  <div class="video-container">
    <?php 
      // Get the Video Fields
      $videoMp4 =  get_field('video_background', CURR_ID); // MP4 Field Name
      $mobileVideoMp4 =  get_field('mobile_video_background'); // MP4 Field Name
    ?>
    <?= get_field('overlay_background_color',CURR_ID) ? '<div class="overlay" style="background:' . get_field('overlay_background_color',CURR_ID) . ';"></div>' : ''; ?>
    <video class="show-dktp" loop muted playsinline autoplay>
      <source src="<?php echo $videoMp4['url']; ?>" type="video/mp4">
    </video>
    <video class="show-mbl" loop muted playsinline autoplay>
      <source src="<?php echo $mobileVideoMp4['url']; ?>" type="video/mp4">
    </video>
    <!-- <div class="show-dktp" style="background-image: url(<?//= get_template_directory_uri();?>/src/images/contact-us.png)"></div>
    <div class="show-mbl" style="background-image: url(<?//= get_template_directory_uri();?>/src/images/contact-us-m.png)"></div> -->
  </div>
  <div class="flex-column semi-full-width">
    <div class="left">
      <div class="title-wrapper">
      <span <?php echo get_field('line_color', CURR_ID) ? 'style="background:' . get_field('line_color', CURR_ID) . ';"' : ''; ?>></span>
      <?php if(get_field('main_title', CURR_ID)):?>
        <?php echo get_field('main_title', CURR_ID); ?>
      <?php endif; ?>
      <?php if(get_field('tagline', CURR_ID)):?>
        <?php echo get_field('tagline', CURR_ID); ?>
      <?php endif; ?>
    </div>
    </div>
    <div class="right">
      <?php echo do_shortcode('[contact-form-7 id="519" title="Contact Form"]'); ?>
    </div>
  </div>
</section>