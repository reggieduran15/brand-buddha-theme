<?php 
  if(class_exists('acf')) {
    if(get_field('title_informed')) {
      $informedTitle = get_field('title_informed');
    }
    if ( get_field( 'back_to_top_icon', 'option' ) ) {
      $backToTopIcon = get_field( 'back_to_top_icon', 'option' );
    }
  }
?>
<section class="informed">
  <div class="top-title">
    <?php echo $informedTitle; ?>
  </div>
  <div class="container">
    <div class="sidebar-wrapper show-dktp"></div>
    <div id="main-slider" class="slider-wrapper">
      <div class="image-slider owl-carousel" id="slider">
      <?php
        if( have_rows('slider_content', CURR_ID) ):
          $count = 0;
          while( have_rows('slider_content', CURR_ID) ) : the_row();
            $count++;
            $image = get_sub_field('slider_image', CURR_ID);
            ?>
            <div class="slider-list a<?php echo $count;?>">
              <div class="image">
                <img src="<?php echo esc_url($image['url']); ?>" alt="">
              </div>
            </div>
            <?php
          endwhile;
        endif;
      ?>
      </div>
      <div class="text-slider owl-carousel with-line" id="slider">
      <?php
        if( have_rows('slider_content', CURR_ID) ):
          $count = 0;
          while( have_rows('slider_content', CURR_ID) ) : the_row();
            $count++;
            $title = get_sub_field('slider_title', CURR_ID);
            $content = get_sub_field('slider_content', CURR_ID);
            ?>
            <div class="slider-list a<?php echo $count;?>">
               <div class="content">
                <div>
                  <?php echo $title; ?>
                </div>
                <?php echo $content; ?>
              </div> 
            </div>
            <?php
           endwhile;
         endif;
      ?>
      </div>
    </div>
  </div>
  <div class="button-container">
    <div id="scroll-down" class="up-down-icon">
      <a href="#case-study"></a>
      <p id="scroll-down" class="up-down-text">Scroll Down</p>
      <img src="<?php echo esc_url($backToTopIcon['url']); ?>" alt="">
    </div>
  </div>
</section>