<section id="case-study" class="case-study full-width with-margin">
  <?php if ( get_field('title_cs', CURR_ID) ) :  ?>
    <div class="top-title">
      <?php echo get_field('title_cs'); ?>
    </div>
  <?php endif; ?>
  <div class="cs-wrapper">
    <?php $loop = new WP_Query( array( 'post_type' => 'case_study', 'posts_per_page' => 4, 'order' => 'ASC' ) ); ?>
    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
      <div class="custom-post">
        <div class="custom-post-container">
          <div class="content-overlay"></div>
          <div class="content">
            <span class="line" style="background: <?php echo get_field('line_color', get_the_ID()) ? get_field('line_color', get_the_ID()) : '#ffcc05' ?>;"></span>
            <?php the_title( '<h3 class="entry-title"><a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '" rel="bookmark">', '</a></h3>' ); ?>
            <div class="entry-content">
              <?php if (has_excerpt()) {
                $excerpt = wp_strip_all_tags(get_the_excerpt());
              } ?>
              <?php //the_content(); 
                echo $excerpt;
              ?>
            </div>
            <div class="button-hover">
              <a class="button" href="<?php echo get_permalink(); ?>">View Case Study</a>
              <span></span>
            </div>
          </div>

          <?php 
            /* grab the url for the full size featured image */
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
      
            /* link thumbnail to full size image for use with lightbox*/
            echo '<a href="'. get_permalink() .'" rel="lightbox"></a>'; 
          ?>
          <div class="image-url" style="background-image: url(<?php echo $featured_img_url; ?>"></div>
        </div>
      </div>
    <?php endwhile; ?>
  </div>
  <?php if ( get_field('button_text_cs', CURR_ID) ) :  ?>
    <div class="button-hover semi-full-width white-bg">
      <a class="button" href="<?php echo get_field('button_link_cs', CURR_ID) ? get_field('button_link_cs', CURR_ID) : '#'; ?>"><?php echo get_field('button_text_cs', CURR_ID); ?></a>
      <span></span>
    </div>
  <?php endif; ?>
  <!-- <img class="hide-mobile" src="<?php //echo get_template_directory_uri(); ?>/src/images/dots-icon5.png" alt="" /> -->
</section>