<?php 
  if(class_exists('acf')) {
    if(get_field('content_title')) {
      $contentTitle = get_field('content_title');
    }
    if(get_field('content_text')) {
      $contentText = get_field('content_text');
    }
    if(get_field('parallax_image')) {
      $parallaxImage = get_field('parallax_image');
    }
    if(get_field('button_text')) {
      $buttonText = get_field('button_text');
    }
    if(get_field('button_link')) {
      $buttonLink = get_field('button_link');
    }
    if(get_field('button_color')) {
      $buttonColor = get_field('button_color');
    }
    if(get_field('button_text_color')) {
      $buttonTextColor = get_field('button_text_color');
    }
  }
?>

<section class="get-in-touch">
  <div class="sidebar-wrapper show-dktp">
    <?php get_template_part('template-parts/sidebar-icons'); ?>
  </div>
  <div class="content-wrapper">
    <div class="bg-image"></div>
    <div class="content">
      <div class="with-line white">
        <?php echo $contentTitle; ?>
      </div>
      <p><?php echo $contentText; ?></p>
      <div class="button-hover yellow-bg">
        <a class="button" href="<?php echo $buttonLink; ?>"><?php echo $buttonText; ?></a>
        <span></span>
      </div>
    </div>
    <img class="glass show-dktp" src="<?php echo $parallaxImage['url'];?>" alt="">
  </div>
</section>