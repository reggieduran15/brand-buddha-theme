<?php 
  // Get the Video Fields
  $videoMp4 =  get_field('mp4_video'); // MP4 Field Name
  $mobileVideoMp4 =  get_field('mobile_mp4_video'); // MP4 Field Name
?>

<section class="top-video">
  <div class="overlay"></div>
  <video class="show-dktp" poster="<?php echo get_template_directory_uri(); ?>/src/images/banner-img-plcholder.png" loop autoplay muted playsinline>
    <source src="<?php echo $videoMp4['url']; ?>" type="video/mp4">
  </video>
  <video class="show-mbl" poster="<?php echo get_template_directory_uri(); ?>/src/images/banner-plchlder-mobile.png" loop autoplay muted playsinline>
    <source src="<?php echo $mobileVideoMp4['url']; ?>" type="video/mp4">
  </video>
  <button type="button" id="play_button">
    <span>Play Reel</span>
    <img src="<?php echo get_template_directory_uri(); ?>/src/images/play-btn.svg" alt="">
  </button>
</section>