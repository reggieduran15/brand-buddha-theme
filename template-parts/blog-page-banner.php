<?php 
$blog_ID = get_option( 'page_for_posts' );
$thumbnail = get_the_post_thumbnail_url($blog_ID, 'full');
?>
<section class="blog-post-banner">
  <div class="bg-image" style="background-image:url(<?php echo $thumbnail; ?>)"></div>
  <div class="container">
    <?php if (get_field('banner_title', $blog_ID)) : ?>
      <div class="title with-line white">
        <?php echo get_field('banner_title', $blog_ID); ?>
      </div>
    <?php endif; ?>
    <?php if (get_field('button_text', $blog_ID)) : ?>
      <span class="btn-wrapper button-hover" <?php echo get_field('button_background_color', $blog_ID) ? 'style="background: ' . get_field('button_background_color', $blog_ID) . ';"' : ''; ?>>
        <a>
          <?php echo get_field('button_text', $blog_ID); ?>
        </a>
        <span></span>
      </span>
    <?php endif; ?>
  </div>
</section>