<section class="our-clients">
  <div class="container">
    <?php if ( get_field('title_oc', CURR_ID) ) :  ?>
      <div class="title with-line"><?php echo get_field('title_oc', CURR_ID); ?></div>
    <?php endif; ?>
    <?php

    // Check rows exists.
    if( have_rows('upload_logos') ):
      ?> <div class="logos"> <?php
        // Loop through rows.
        while( have_rows('upload_logos') ) : the_row();

            // Do something...
            if(get_sub_field('logo')) : ?>
            <div class="img-wrapper">
              <img src="<?php echo get_sub_field('logo')['url']?>" alt="">
            </div>
            <?php endif; 
        // End loop.
        endwhile;

    // No value.
    else :
      // Do something...
      ?> </div> <?php
    endif;
    ?>
  </div>
</section>