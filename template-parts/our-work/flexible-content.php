<section class="ow-flex-content">
  <div class="container">
  <?php
    // check if the flexible content field has rows of data
    if( get_field('flexible_content') ):
      // loop through the rows of data
      while ( has_sub_field('flexible_content') ) :
        if( get_row_layout() == 'two_column_text' ):
          ?>
          <section class="two-col-text animated hiding" data-animation="fadeInUp" data-delay="300">
            <div class="column left">
              <?php if ( get_sub_field('left_content_text') ) { 
                echo get_sub_field('left_content_text', CURR_ID );
              } ?>
            </div>
            <div class="column right">
              <?php if ( get_sub_field('right_content_text', CURR_ID ) ) : 
                echo get_sub_field('right_content_text', CURR_ID );
              endif; ?>
            </div>
          </section>
        <?php elseif (get_row_layout() == 'image_&_text') : ?>
          <section class="semi-full-width image-text animated hiding" data-animation="fadeInUp" data-delay="300">
            <div class="column left">
              <?php if ( get_sub_field('upload_image') ) { ?>
                <img src="<?php echo get_sub_field('upload_image', CURR_ID )['url']; ?>" alt="">
              <?php } ?>
            </div>
            <div class="column right">
              <div class="text-wrapper">
                <span class="line" style="background: <?php echo get_sub_field('top_line_color', CURR_ID) ? get_sub_field('top_line_color', CURR_ID) : '#ffcc05' ?>;"></span>
                <?php if ( get_sub_field('text_content', CURR_ID ) ) : 
                  echo get_sub_field('text_content', CURR_ID );
                endif; ?>
              </div>
            </div>
          </section>
        <?php elseif (get_row_layout() == 'image_&_image_with_text') : ?>
          <section class="semi-full-width two-image-text animated hiding <?php echo get_sub_field('reverse_column') == 'Yes' ? 'reverse-col' : ''; ?>" data-animation="fadeInUp" data-delay="300">
            <div class="top">
              <div class="column left">
                <?php if ( get_sub_field('upload_left_image') ) { ?>
                  <img src="<?php echo get_sub_field('upload_left_image', CURR_ID )['url']; ?>" alt="">
                <?php } ?>
              </div>
              <div class="column right">
                <?php if ( get_sub_field('upload_right_image') ) { ?>
                  <img src="<?php echo get_sub_field('upload_right_image', CURR_ID )['url']; ?>" alt="">
                <?php } ?>
              </div>
            </div>
            <div class="bottom">
              <div class="blank show-dktp"></div>
              <div class="text-wrapper">
                <span class="line" style="background: <?php echo get_sub_field('top_line_color', CURR_ID) ? get_sub_field('top_line_color', CURR_ID) : '#ffcc05' ?>;"></span>
                <?php if ( get_sub_field('text_content', CURR_ID ) ) : 
                  echo get_sub_field('text_content', CURR_ID );
                endif; ?>
              </div>
            </div>
          </section>
        <?php elseif (get_row_layout() == 'text_with_button_&_image') : ?>
          <section class="one-third text-btn-image animated hiding <?php echo get_sub_field('reverse_column') == 'Yes' ? 'reverse-col' : ''; ?>" data-animation="fadeInUp" data-delay="300">
            <div class="column left">
              <div class="text-wrapper">
                <span class="line" style="background: <?php echo get_sub_field('top_line_color', CURR_ID) ? get_sub_field('top_line_color', CURR_ID) : '#ffcc05' ?>;"></span>
                <?php if ( get_sub_field('text_content', CURR_ID ) ) : 
                  echo get_sub_field('text_content', CURR_ID );
                endif; ?>
              </div>
              <?php if ( get_sub_field('button_text') ) { ?>
                <div class="button-hover">
                  <a class="button" href="<?php echo get_sub_field('button_link') ? get_sub_field('button_link') : '#'; ?>">
                    <?php echo get_sub_field('button_text', CURR_ID ); ?>
                  </a>
                  <span></span>
                </div>
              <?php } ?>
            </div>
            <div class="column right">
              <?php if ( get_sub_field('upload_image') ) { ?>
                <img src="<?php echo get_sub_field('upload_image', CURR_ID )['url']; ?>" alt="">
              <?php } ?>
              <?php if ( get_sub_field('button_text') ) { ?>
                <div class="button-hover">
                  <a class="button" href="<?php echo get_sub_field('button_link') ? get_sub_field('button_link') : '#'; ?>">
                    <?php echo get_sub_field('button_text', CURR_ID ); ?>
                  </a>
                  <span></span>
                </div>
              <?php } ?>
            </div>
          </section>
        <?php endif;
      endwhile;
    endif;
  ?>
  </div>
</section>
