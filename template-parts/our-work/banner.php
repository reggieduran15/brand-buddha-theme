<section class="our-work-banner full-width" id="our-work-banner">
  <?php if (get_field('background_image', CURR_ID)) : ?>
    <div class="bg-image" style="background-image: url(<?php echo get_field('background_image', CURR_ID)['url'];?>)"></div>
    <img class="show-dktp" src="<?php echo get_template_directory_uri();?>/src/images/Lotus-dktp.png" alt="">
    <img class="show-mbl" src="<?php echo get_template_directory_uri();?>/src/images/Lotus.png" alt="">
  <?php endif; ?>
  <div class="container semi-full-width">
    <?php if (get_field('main_title', CURR_ID)) : ?>
      <div class="title with-line white">
        <?php echo get_field('main_title', CURR_ID); ?>
      </div>
    <?php endif; ?>
  </div>
</section>