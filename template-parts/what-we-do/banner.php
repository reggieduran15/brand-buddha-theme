<section class="wwd-banner full-width" id="wwd-banner">
  <?php 
    // Get the Video Fields
    $videoMp4 =  get_field('video_banner', CURR_ID); // MP4 Field Name
    $mobileVideoMp4 =  get_field('mobile_video_banner'); // MP4 Field Name
  ?>
  <?php if(get_field('overlay_background_color', CURR_ID)) : ?>
    <div class="overlay" <?php echo get_field('overlay_background_color', CURR_ID) ? 'style="background:' . get_field('overlay_background_color', CURR_ID) . ';"' : ''; ?>></div>
  <?php endif; ?>
  <video class="show-dktp" loop muted playsinline autoplay>
    <source src="<?php echo $videoMp4['url']; ?>" type="video/mp4">
  </video>
  <video class="show-mbl" loop muted playsinline autoplay>
    <source src="<?php echo $mobileVideoMp4['url']; ?>" type="video/mp4">
  </video>
  <?php //if (get_field('background_image', CURR_ID)) : ?>
    <!-- <div class="bg-image" style="background-image: url(<?php //echo get_field('background_image', CURR_ID)['url'];?>)"></div> -->
  <?php //endif; ?>
  <div class="container semi-full-width">
    <?php if (get_field('main_title', CURR_ID)) : ?>
      <div class="title with-line white">
        <?php echo get_field('main_title', CURR_ID); ?>
      </div>
    <?php endif; ?>
  </div>
  
</section>