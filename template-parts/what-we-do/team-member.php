<section id="wwd-tm" class="wwd-tm">
  <div class="container" id="team-listings">
    <!-- <?php if (have_rows('team_member_details', CURR_ID)) : 
      while(have_rows('team_member_details', CURR_ID)) : the_row();
        ?> 
        <div class="tm-wrapper">
          <img class="bnw-avatar" src="<?php echo get_sub_field('bnw_avatar', CURR_ID)['url']; ?>" alt="">
          <img class="colored-avatar" src="<?php echo get_sub_field('colored_avatar', CURR_ID)['url']; ?>" alt="">
          <div class="info-wrapper">
            <span class="name">
              <?php echo get_sub_field('name', CURR_ID); ?>
            </span>
            <span class="pos">
              <?php echo get_sub_field('position', CURR_ID); ?>
            </span>
          </div>
        </div>
        <?php
      endwhile;
    endif; ?> -->

    <?php $loop = new WP_Query( array( 'post_type' => 'team_member', 'posts_per_page' => 3, 'order' => 'DATE' ) ); ?>
    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
      <div class="tm-wrapper">
        <img class="bnw-avatar" src="<?php echo get_field('black_and_white_avatar')['url']; ?>" alt="">
        <img class="colored-avatar" src="<?php echo get_field('colored_avatar')['url']; ?>" alt="">
        <div class="info-wrapper">
          <span class="name">
            <?php the_title(); ?>
          </span>
          <?php if ( get_field('position') ) : ?>
            <span class="pos">
              <?php echo get_field('position'); ?>
            </span>
          <?php endif; ?>
        </div>
      </div>
    <?php endwhile; ?>
  </div>
  <div class="more-tm-btn">
		<div id="more-people" class="btn-wrapper">
      <span>Meet More People</span>
			<img src="<?php echo get_template_directory_uri();?>/src/images/more-btn.png" alt="">
		</div>
  </div>
</section>