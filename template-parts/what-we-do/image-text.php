<section class="wwd-image-text-section" id="wwd-image-text">
  <div class="container">
  <?php
    if( have_rows('image_&_text', CURR_ID) ):
      while( have_rows('image_&_text', CURR_ID) ) : the_row();
        ?> 
        <section id="wwd-image-text-<?php echo get_row_index(); ?>" class="wwd-image-text <?php echo get_sub_field('reverse') == 'Yes' ? 'reverse-col' : ''?>" style="background: <?php echo get_sub_field('background_color', CURR_ID) ? get_sub_field('background_color', CURR_ID) : '#ffffff';?>">
          <div class="content-wrapper">
            <div class="column left">
              <?php 
                if (get_sub_field('content', CURR_ID)) :
                  echo get_sub_field('content', CURR_ID);
                endif;
              ?>
            </div>
            <div class="column right">
              <div class="container">
                <?php 
                  // Get the Video Fields
                  $videoMp4 =  get_sub_field('upload_video', CURR_ID); // MP4 Field Name
                ?>
                <?php if($videoMp4) { ?>
                  <video loop muted playsinline autoplay>
                    <source src="<?php echo $videoMp4['url']; ?>" type="video/mp4">
                  </video>
                  <?php
                    if (get_sub_field('play_button_caption')) :
                      ?>
                      <button type="button" id="play_button">
                        <span style="border-color:<?php echo get_sub_field('line_color');?>">
                          <?php echo get_sub_field('play_button_caption'); ?>
                        </span>
                        <?php echo get_sub_field('play_button_icon') ? '<img src="' . get_sub_field('play_button_icon')['url'] . '" alt="">' : ''; ?>
                      </button>
                      <?php
                    endif;
                  ?>
                <?php } else { 
                  if (get_sub_field('upload_image', CURR_ID)) :
                    ?> <img src="<?php echo get_sub_field('upload_image', CURR_ID)['url']; ?>" alt=""> <?php
                  endif;
                } ?>
              </div>
              
            </div>
          </div>
        </section>
        <?php
      endwhile;
    endif;
  ?>
  </div>
</section>