<section class="wwd-about full-width" id="wwd-about">
  <?php $bgColor =  get_field("background_color", CURR_ID) ? 'style="background: ' . get_field('background_color', CURR_ID) . '"' : '' ?>
  <div class="bg-color" <?php echo $bgColor; ?>></div>
  <div class="container">
    <?php if (get_field('content', CURR_ID)) :?>
      <?php echo get_field('content', CURR_ID); ?>
    <?php endif; ?>
  </div>
</section>