<div class="sidebar-icons">
	<?php
		if( have_rows('social_icons', 'options') ):
			while( have_rows('social_icons', 'options') ) : the_row();
				$icon = get_sub_field('upload_icon');
				$link = get_sub_field('link');
				?>
				<div class="icon-wrapper">
					<a href="<?php echo $link; ?>">
					  <img src="<?php echo esc_url($icon['url']); ?>" alt="">
          </a>
				</div>
				<?php
			endwhile;
		endif;
	?>
</div>
