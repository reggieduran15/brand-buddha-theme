<section class="contact-us-section">
  <div class="semi-full-width">
    <div class="background"></div>
    <div class="column left">
      <?php if(get_field('title_cu', 'options')) : ?>
        <div class="with-line black">
          <?php echo get_field('title_cu', 'options'); ?>
        </div>
      <?php endif; ?>
      <?php if(get_field('button_text_cu', 'options')) : ?>
        <div class="button-hover yellow-bg show-dktp">
          <a class="button" href="<?php echo get_field('button_link_cu', 'options'); ?>"><?php echo get_field('button_text_cu', 'options'); ?></a>
          <span></span>
        </div>
      <?php endif; ?>
      <div class="social-wrapper show-dktp">
        <?php
        // Check rows exists.
        if( have_rows('social_icons_cu', 'options') ):

          // Loop through rows.
          while( have_rows('social_icons_cu', 'options') ) : the_row();
            // Load sub field value.
            $icon = get_sub_field('upload_icon', 'options');
            $link = get_sub_field('social_link', 'options');
            // Do something...
            ?>
            <div class="icon-wrapper">
              <a href="<?php echo $link; ?>">
                <img src="<?php echo esc_url($icon['url']); ?>" alt="">
              </a>
            </div>
            <?php

          // End loop.
          endwhile;
        // No value.
        else :
          // Do something...
        endif;
        ?>
      </div>
    </div>

    <div class="column right">
      <div class="info-wrapper">
        <?php if(get_field('tel_no_icon', 'options')) : ?>
          <a href="tel:<?php echo str_replace(".", "-", get_field('tel_no', 'options'));?>">
            <img class="show-dktp" src="<?php echo get_field('tel_no_icon', 'options')['url']; ?>" alt="">
            <span><?php echo get_field('tel_no', 'options');?></span>
          </a>
        <?php endif; ?>
      </div>
      <div class="info-wrapper">
        <?php if(get_field('email_icon', 'options')) : ?>
          <a href="mailto:<?php echo get_field('email_address', 'options');?>">
            <img class="show-dktp" src="<?php echo get_field('email_icon', 'options')['url']; ?>" alt="">
            <span><?php echo get_field('email_address', 'options');?></span>
          </a>
        <?php endif; ?>
      </div>
      <div class="info-wrapper">
        <?php if(get_field('map_icon', 'options')) : ?>
          <a href="#">
            <img class="show-dktp" src="<?php echo get_field('map_icon', 'options')['url']; ?>" alt="">
            <span><?php echo get_field('map_address', 'options');?></span>
          </a>
        <?php endif; ?>
      </div>
   
      <div class="social-wrapper">
        <?php
        // Check rows exists.
        if( have_rows('social_icons_cu', 'options') ):

          // Loop through rows.
          while( have_rows('social_icons_cu', 'options') ) : the_row();
            // Load sub field value.
            $icon = get_sub_field('upload_icon', 'options');
            $link = get_sub_field('social_link', 'options');
            // Do something...
            ?>
            <div class="icon-wrapper">
              <a href="<?php echo $link; ?>">
                <img src="<?php echo esc_url($icon['url']); ?>" alt="">
              </a>
            </div>
            <?php
          // End loop.
          endwhile;
        // No value.
        else :
          // Do something...
        endif;
        ?>
      </div>
      <?php if(get_field('button_text_cu', 'options')) : ?>
        <div class="button-hover yellow-bg show-mbl">
          <a class="button" href="<?php echo get_field('button_link_cu', 'options'); ?>"><?php echo get_field('button_text_cu', 'options'); ?></a>
          <span></span>
        </div>
      <?php endif;?>
    </div>
  </div>
</section>