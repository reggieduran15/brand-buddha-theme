<?php
get_header();
?>
<div id="main-wrapper" class="home">
  <?php  if ( is_blog() ) {
    define('CURR_ID',  get_option( 'page_for_posts' ));

    get_template_part('template-parts/blog-page-banner');

    get_template_part('template-parts/blog-posts');
    
    get_template_part('template-parts/contact-us');
  } ?>
</div>
<?php get_footer(); ?>