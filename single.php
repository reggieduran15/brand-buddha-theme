<?php
/**
 * The template for displaying all single posts and attachments
 */
?>
<?php 
get_header(); 
define('CURR_ID', get_the_ID());
get_template_part('template-parts/single-post/banner');
?>
<section id="primary" class="content-area container blog-detailed-section">
  <main id="main" class="site-main " role="main">
    <section class="full-width top-section">
      <?php if(have_posts()): ?>
        <?php while(have_posts()) : the_post(); ?>
          <div class="semi-full-width single-post-section">
            <div class="content-details">
              <div class="sps--content">
                <?php the_content(); ?>
              </div>
              <?php get_template_part('template-parts/single-post/recent-post'); ?>
              <?php get_template_part('template-parts/single-post/post-nav'); ?>
            </div>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
    </section>
  </main><!-- .site-main -->
</section><!-- .content-area -->
<?php get_template_part('template-parts/contact-us'); ?>
<?php get_footer(); ?>