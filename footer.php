    </section>
    <footer>
      <?php
      if ( class_exists( 'acf' ) ) {
        if ( get_field( 'footer_logo', 'option' ) ) {
          $footerLogo = get_field( 'footer_logo', 'option' );
        }
        if ( get_field( 'back_to_top_icon', 'option' ) ) {
          $backToTopIcon = get_field( 'back_to_top_icon', 'option' );
        }
      } 
      ?>
      <div class="wrapper-logo-cr">
        <div class="footer-logo">
          <a href="<?php echo get_site_url(); ?>/" class="logo"></a>
          <img src="<?php echo esc_url($footerLogo['url']); ?>" alt="">
        </div>
        <p>©2020 Brand Buddha Inc. All Rights Reserved.</p>
      </div>
      <div class="wrapper-menu">
        <div class="nav-desktop">
          <?php 
              wp_nav_menu( array(
              'menu' => 'Main Menu',
              'menu_class' => 'header-menu',
              ) );
          ?>
        </div>
        <div id="back-to-top" class="up-down-icon">
          <p id="back-to-top" class="up-down-text">Back to top</p>
          <img src="<?php echo esc_url($backToTopIcon['url']); ?>" alt="">
        </div>
      </div>
    </footer>
    <script src="<?php echo get_template_directory_uri(); ?>/src/js/vendor/jquery.v3.4.1.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/src/js/vendor/jquery.waypoints.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/dist/js/appear.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/dist/js/vendor.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/dist/js/main.min.js"></script>
    <?php wp_footer(); ?>
</body>	
</html>