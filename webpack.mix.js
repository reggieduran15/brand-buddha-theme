const mix = require('laravel-mix');

mix.sass('src/styles/scss/style.scss', 'dist/styles/style.min.css')
    .options({
        processCssUrls: false,
        autoprefixer: {
            options: {
                browsers: [
                    'last 6 versions',
                ]
            }
        }
    });

mix.js('src/js/scripts.js', 'dist/js/main.min.js');

mix.js([
    'src/js/vendor/jquery.v3.4.1.min.js',
    'src/js/vendor/html5.js',
    'src/js/vendor/owl.carousel.min.js',
    'src/js/vendor/TweenMax.min.js'
], 'dist/js/vendor.js');

