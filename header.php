<!doctype html>
<html class="no-js" lang="en">
<head>

<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600&display=swap" rel="stylesheet">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/dist/styles/style.min.css" media="all"/>
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/> -->

<!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.slick/1.5.9/slick.css"/> -->
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css" />
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css" />
<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>


<!-- <div id="preloader">
	<div class="overlay"></div>
	<div class="preloader-wrap">
		<div class="logo">
			<img src="<?php echo get_template_directory_uri(); ?>/src/images/logo.png" width="170" height="auto" alt="" title="">
		</div>
		
		<div class="loader">
			<img src="<?php echo get_template_directory_uri(); ?>/src/images/loader.svg" width="30" height="auto" alt="" title="">
		</div>
	</div>
</div> -->

<section id="main-container">
	<header class="sticky-header">
		<?php 
		if ( class_exists( 'acf' ) ) {
			if ( get_field( 'logo', 'option' ) ) {
			$logo = get_field( 'logo', 'option' );
			}
			if ( get_field( 'sticky_logo', 'option' ) ) {
			$stickyLogo = get_field( 'sticky_logo', 'option' );
			}
			if ( get_field( 'mobile_logo', 'option' ) ) {
			$mobileLogo = get_field( 'mobile_logo', 'option' );
			}
			if ( get_field( 'facebook', 'option' ) ) {
			$facebook = get_field( 'facebook', 'option' );
			}
			if ( get_field( 'twitter', 'option' ) ) {
			$twitter = get_field( 'twitter', 'option' );
			}
			if ( get_field( 'instagram', 'option' ) ) {
			$instagram = get_field( 'instagram', 'option' );
			}
			if ( get_field( 'linkedin', 'option' ) ) {
			$linkedin = get_field( 'linkedin', 'option' );
			}
		}
		?>
    <div class="logo-menu-wrapper">
      <div class="logo">
      <a href="<?php echo get_site_url(); ?>/" class="logo"></a>
      <img class="sticky-logo show-dktp" src="<?php echo esc_url($stickyLogo['url']); ?>">
      <img class="show-dktp main" src="<?php echo esc_url($logo['url']) ?>">
      <img class="show-mbl" src="<?php echo esc_url($mobileLogo['url']) ?>">
      </div>

      <div class="nav-desktop show-dktp">
      <?php 
        wp_nav_menu( array(
        'menu' => 'Main Menu',
        'menu_class' => 'header-menu',
        ) );
      ?>
      </div>
    </div>

    <div class="menu-hamburger">
      <span></span>
    </div>

    <div class="nav-mobile show-mbl">
      <div class="overlay"></div>
      <?php 
      wp_nav_menu( array(
        'menu' => 'Mobile Menu',
        'menu_class' => 'header-menu',
      ) );
      ?>
      <!-- <div class="button-hover white-bg show-mbl">
        <a href="<?php //echo get_site_url(); ?>/contact-us" class="contact-us-btn">Contact us</a>
        <span></span>
      </div> -->
      <div class="social-icons">
      <ul>
        <li class="menu-icon-fb"><a href="#"></a><img src="<?php echo esc_url($facebook['url']); ?>" alt=""></li>
        <li class="menu-icon-tw"><a href="#"></a><img src="<?php echo esc_url($twitter['url']); ?>" alt=""></li>
        <li class="menu-icon-ig"><a href="#"></a><img src="<?php echo esc_url($instagram['url']); ?>" alt=""></li>
        <li class="menu-icon-li"><a href="#"></a><img src="<?php echo esc_url($linkedin['url']); ?>" alt=""></li>
      </ul>
      </div>
    </div>
    <div class="button-hover white-bg show-dktp">
      <a href="<?php echo get_site_url(); ?>/contact-us" class="contact-us-btn">Contact us</a>
      <span></span>
    </div>
	</header>