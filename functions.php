<?php
  
// FEATURED IMAGE
add_theme_support('post-thumbnails');

// MAIN MENU
function register_my_menu() {
    register_nav_menu( 'primary', __( 'Main Menu', 'theme-slug' ) );
}

add_action( 'after_setup_theme', 'register_my_menu' );

if( function_exists('acf_add_options_page') ) {
  acf_add_options_page('General Settings');
}

function is_blog () {
  return ( is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();
}

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// Filter Blog Posts Ajax Function.
add_action( 'wp_enqueue_scripts', 'blog_filter_scripts' );
function blog_filter_scripts() {

  global $wp_query; 
  wp_enqueue_script('jquery');
  wp_register_script( 'blog_filter', get_stylesheet_directory_uri() . '/dist/js/ajax/blog-filter-ajax.js', array('jquery') );

  wp_localize_script( 'blog_filter', 'blog_filter_params', array(
      'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php',
      'posts' => json_encode( $wp_query->query_vars ),
      'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
      'max_page' => $wp_query->max_num_pages
  ) );

  wp_enqueue_script( 'blog_filter' );
}
add_action('wp_ajax_blogfilter', 'blog_filter_ajax_handler');
add_action('wp_ajax_nopriv_blogfilter', 'blog_filter_ajax_handler');
function blog_filter_ajax_handler(){
  global $post;
  $category = $_POST['category'];
  $args = array(
    'post_type'=>'post', 
    'posts_per_page'=> 5, 
    'post_status' => 'publish', 
    'order' => 'DATE', 
    'cat' => intval($category),
    // 'offset'=> $_POST['offset'], 
    // 'paged' => $_POST['page'],
  );        
  $rst=[];
  $query = new WP_Query($args);
  if($query->have_posts()):
      $ix = 0;
      while($query->have_posts()):$query->the_post();
          $count = $query->post_count;
          $p = $post;
          $p->post_title = get_the_title();
          $p->permalink = get_permalink($post->ID);
          $p->datetime = get_the_date('c');
          $p->date = get_the_date('n.j.Y');
          $p->line_color = get_field('line_color',get_the_ID());
          $p->post_featured_image = get_the_post_thumbnail_url(get_the_ID());
          $p->post_count = $count;
          $rst[$ix] = $p;
          $ix++;
      endwhile;
      wp_reset_postdata();
  endif;
  wp_send_json_success(array(
    'post'=>$rst,
    // 'offset'=>$offset
  ));
  die();
}

// Blog Posts Ajax Function.
add_action( 'wp_enqueue_scripts', 'blog_my_load_more_scripts' );
function blog_my_load_more_scripts() {

  global $wp_query; 
  wp_enqueue_script('jquery');
  wp_register_script( 'my_blogloadmore', get_stylesheet_directory_uri() . '/dist/js/ajax/blog-ajax.js', array('jquery') );

  wp_localize_script( 'my_blogloadmore', 'blog_loadmore_params', array(
      'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php',
      'posts' => json_encode( $wp_query->query_vars ),
      'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
      'max_page' => $wp_query->max_num_pages
  ) );

  wp_enqueue_script( 'my_blogloadmore' );
}

add_action('wp_ajax_blogloadmore', 'blog_loadmore_ajax_handler');
add_action('wp_ajax_nopriv_blogloadmore', 'blog_loadmore_ajax_handler');
function blog_loadmore_ajax_handler(){
  global $post;

  $args = array('post_type'=>'post', 'posts_per_page'=> 5, 'post_status' => 'publish', 'offset'=> $_POST['offset'], 'order' => 'DATE', 'paged' => $_POST['page'] );        

  $rst=[];
  $query = new WP_Query($args);

  if($query->have_posts()):
      $ix = 0;
      while($query->have_posts()):$query->the_post();
          $p = $post;
          $p->post_title = get_the_title();
          $p->permalink = get_permalink($post->ID);
          $p->datetime = get_the_date('c');
          $p->date = get_the_date('n.j.Y');
          $p->line_color = get_field('line_color',get_the_ID());
          $p->post_featured_image = get_the_post_thumbnail_url(get_the_ID());
          $rst[$ix] = $p;
          $ix++;
      endwhile;
      wp_reset_postdata();
      $offset = $_POST['offset']+6;
  endif;
  wp_send_json_success(array('post'=>$rst, 'offset'=>$offset));
  die();
}


// Team Member Ajax Function
add_action( 'wp_enqueue_scripts', 'team_loadmore_scripts' );
function team_loadmore_scripts() {

  global $wp_query; 
  wp_enqueue_script('jquery');
  wp_register_script( 'my_teamloadmore', get_stylesheet_directory_uri() . '/dist/js/ajax/team-ajax.js', array('jquery') );

  wp_localize_script( 'my_teamloadmore', 'team_loadmore_params', array(
      'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php',
      'posts' => json_encode( $wp_query->query_vars ),
      'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
      'max_page' => $wp_query->max_num_pages
  ) );

  wp_enqueue_script( 'my_teamloadmore' );
}

add_action('wp_ajax_teamloadmore', 'team_loadmore_ajax_handler');
add_action('wp_ajax_nopriv_teamloadmore', 'team_loadmore_ajax_handler');
function team_loadmore_ajax_handler(){
  global $post;
  $args = array(
    'post_type'=>'team_member',
    'posts_per_page'=> 3,
    'post_status' => 'publish',
    'offset'=> $_POST['offset'],
    'order' => 'DATE',
    'paged' => $_POST['page']
  );

  $rst=[];
  $query = new WP_Query($args);
  if($query->have_posts()):
      $ix = 0;
      while($query->have_posts()):$query->the_post();
          $p = $post;
          $p->post_title = get_the_title();
          $p->permalink = get_permalink($post->ID);
          $p->datetime = get_the_date('c');
          $p->date = get_the_date('n.j.Y');
          $p->pos = get_field('position',get_the_ID());
          $p->bnw = get_field('black_and_white_avatar',get_the_ID())['url'];
          $p->colored = get_field('colored_avatar',get_the_ID())['url'];
          $p->post_featured_image = get_the_post_thumbnail_url(get_the_ID());
          $rst[$ix] = $p;
          $ix++;
      endwhile;
      wp_reset_postdata();
      $offset = $_POST['offset']+3;
  endif;
  wp_send_json_success(array(
    'post'=>$rst, 
    'offset'=>$offset
  ));
  die();
}