const { cssNumber } = require("jquery");
animElmts = document.querySelectorAll( '.two_column_text h3' );
var elementAppear = false;
var lotusAppear = false;

$( window ).on('load', function() {
	initPageAnim();
});

$( window ).scroll(function() {
  parallax();
  if($('#main-slider').length) {
    if ($(window).scrollTop() > $('#main-slider').offset().top - 130) {
      if (!$('#main-slider').hasClass('triggerd_scroll')) { $('#main-slider').addClass('triggerd_scroll'); }    
    } else {        
      if ($('#main-slider').hasClass('triggerd_scroll')) { $('#main-slider').removeClass('triggerd_scroll'); }
    }
  }
});


// Add class in menu hamburger when clicked.
if (document.querySelector(".menu-hamburger")) {
	document
	.querySelector(".menu-hamburger")
	.addEventListener("click", function(e) {
	e.preventDefault();
	if (
		document
		.getElementsByTagName("body")[0]
		.classList.contains("active-header_js")
	) {
		document
		.getElementsByTagName("body")[0]
		.classList.remove("active-header_js");
		document
		.getElementsByTagName("body")[0]
		.classList.remove("no-scroll_js");
	} else {
		document
		.getElementsByTagName("body")[0]
		.classList.add("active-header_js");
		document.getElementsByTagName("body")[0].classList.add("no-scroll_js");
	}
	});
}

$('video').parent().click(function () {
  let vid = 0;
  if($(window).width() < 767) vid = 1
  if($(this).children("video").get(vid).paused){
    $(this).children("video").get(vid).play();
    $(this).children("#play_button").fadeOut();
    $(this).children('.overlay').fadeOut();
  }else{       
    $(this).children("video").get(vid).pause();
    $(this).children("#play_button").fadeIn();
    $(this).children("#play_button").css("display","flex");
    $(this).children('.overlay').fadeIn();
  }
  
});

// When the user scrolls the page, execute myFunction
window.onscroll = function() {onScrollHeader(); }

// Get the header
var header = document.querySelector(".sticky-header");

// Get the offset position of the navbar
var sticky = header.offsetTop;

// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
function onScrollHeader() {
  if (window.pageYOffset >= 1610 && document.querySelector('.slider-wrapper')) {
    document.querySelector('.slider-wrapper').classList.add('scroll');
  } else {
    if( document.querySelector('.slider-wrapper')) {
      document.querySelector('.slider-wrapper').classList.remove('scroll');
    }
  }
  
  if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}

if (jQuery) { $ = jQuery; }
if ($) { jQuery = $; }

function initPageAnim(container) {
	$("body").find(".animated").appear(
		function () {
			var element = $(this);
			var animation = element.data("animation");
			var animationDelay = element.data("delay");

			if (animationDelay) {
				setTimeout(function () {
					element.addClass(animation);
          element.removeClass("hiding");
				}, animationDelay);
			} else {
				element.addClass(animation);
				element.removeClass("hiding");
			}
		}, { accY: -150 }
  );
  $('#num-animate').appear(function(){
		if(!elementAppear){
			setTimeout(animNum, 300);
		}
  },{accY: 100});
  $('.right_column_text .right').appear(function(){
		if(!lotusAppear){
			setTimeout(lotusAddClass, 800);
		}
	},{accY: -700});
}

function parallax() {
	if ($('.glass').length) {
		parallaxBackground($('.glass'), 0.5);
  };
  if ($('.lotus-parallax ').length) {
		lotusParallax($('.lotus-parallax '), 10);
	};
}

function lotusParallax(imgDiv,multiplier) {
  var scrolled = $(window).scrollTop();
	var imgPar = imgDiv;
	var imgParPercY = (imgPar.offset().top + $(window).scrollTop()) / imgPar.outerHeight(false);
  var parallaxHeight = scrolled*0.20;
  var newH = Math.round((imgParPercY) * parallaxHeight * multiplier / 2);	
  TweenMax.to($(imgDiv), .4, {top: -1000 + newH+'px', ease:Power1.easeOut});
  if(scrolled == 0) {
    $('.lotus-parallax').css("opacity","0");
  } else {
    $('.lotus-parallax').css("opacity","1");
  }
}

function parallaxBackground(imgDiv,multiplier) {
  var scrolled = $(window).scrollTop();
	// var wH = window.innerHeight ? window.innerHeight : $(window).height();
	var imgPar = imgDiv;
	var imgParPercY = (imgPar.offset().top + $(window).scrollTop()) / imgPar.outerHeight(false);
  // var parallaxHeight = imgPar.outerHeight(false);
  var parallaxHeight = scrolled*0.15;
  var newH = Math.round((imgParPercY) * parallaxHeight * multiplier / 2);	
  TweenMax.to($(imgDiv), .4, {top: newH+'px', ease:Power1.easeOut});
}

const imageSlide = $('.image-slider');
imageSlide.slick({
  slidesToScroll: 1,
  fade: true,
  infinite: false,
  cssEase: 'linear',
  prevArrow: false,
  speed: 1200,
  nextArrow: false,
  asNavFor: '.text-slider',
});
const textSlide = $('.text-slider');
textSlide.slick({
  slidesToScroll: 1,
  speed: 1200,
  prevArrow: false,
  nextArrow: false,
  vertical: true,
  asNavFor: '.image-slider',
  infinite: false,
  verticalSwiping: true
});

$(window).on('scroll', function() {
  var current_position = $(document).scrollTop();
  var filters = [];
  if(current_position > 800) {
    if($('.right_column_text li').length) {
      $('.right_column_text li').each(function(i) {
        var row = $(this);
        setTimeout(function(){
          row.addClass('active');
        },150*i);
      })
    }
  }
  if($('.right_column_text li.active').length) {
    $('.right_column_text li.active').each(function() {
      var found = jQuery.inArray($(this), filters);
      if (found <= 0)
        filters.push($(this));
    })
  }
  if(current_position < 600) {
    filters.reverse();
    $.each(filters, function(key, value) {
      setTimeout(function(){
        value.removeClass('active');
      },150*key);
    })
  }
});


if ($('.slider-wrapper').length) {
  var waypoint = new Waypoint({
    element:  $('.slider-wrapper'),
    handler: function() {
      // notify('Basic waypoint triggered')
      $('.informed .sidebar-wrapper').on('wheel', function (e) {
        var slideCount = imageSlide[0].slick["slideCount"];
        var currentIndex = imageSlide.slick("slickCurrentSlide");
        var totalSildeToShow = imageSlide[0].slick.options["slidesToShow"];
  
        if (e.originalEvent.deltaY < 0) {
          e.preventDefault();
          imageSlide.slick('slickPrev');
        } else {
          if (slideCount - totalSildeToShow == currentIndex) return;
          e.preventDefault();
          imageSlide.slick('slickNext');
        }
      });
      textSlide.on('wheel',  function (e) {
        var slideCount = $(this)[0].slick["slideCount"];
        var currentIndex = $(this).slick("slickCurrentSlide");
        var totalSildeToShow =  $(this)[0].slick.options["slidesToShow"];
  
        if (e.originalEvent.deltaY < 0) {
          e.preventDefault();
          $(this).slick('slickPrev');
        } else {
          if (slideCount - totalSildeToShow == currentIndex)
            return;
          e.preventDefault();
          $(this).slick('slickNext')
        }
      });
      imageSlide.on('wheel',  function (e) {
        var slideCount = $(this)[0].slick["slideCount"];
        var currentIndex = $(this).slick("slickCurrentSlide");
        var totalSildeToShow =  $(this)[0].slick.options["slidesToShow"];
      
        if (e.originalEvent.deltaY < 0) {
          if(currentIndex == 0) 
            return
          e.preventDefault();
          $(this).slick('slickPrev');
        } else {
          if (slideCount - totalSildeToShow == currentIndex)
            return;
          e.preventDefault();
          $(this).slick('slickNext')
        }
      });
    },
    offset: 130
  })
}


$("#back-to-top").click(function () {
  //1 second of animation time
  //html works for FFX but not Chrome
  //body works for Chrome but not FFX
  //This strange selector seems to work universally
  $("html, body").animate({scrollTop: 0}, 1000);
});


var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("select-wrapper");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;

  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 0; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.setAttribute("data-category", selElmnt.options[j].getAttribute('value'));

    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/

        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;

        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click(s.options[i].innerHTML);

    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");

    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;

  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);

var selectItems = document.querySelectorAll('.select-items div');
if (selectItems) {
  Array.prototype.forEach.call(selectItems, function(selectItem) {
    selectItem.addEventListener("click", function() {
      document.querySelector('.select-selected').setAttribute("data-category", selectItem.getAttribute('data-category'));
    });
  });
}

// Select informed hash to case study
$('.informed a[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top - 160
        }, 1000, function() {
        });
      }
    }
});

function lotusAddClass() {
  lotusAppear = true;
  $('#cs-flexible-content').addClass('active');
}

function animNum() {
  elementAppear = true;
  numAnim(animElmts);
}

function numAnim(elements) {
  if (elements) {
    Array.prototype.forEach.call( elements, function( el, i ) {
  
      // If no number, don't do anything.
      if ( ! /[0-9]/.test( el.innerHTML ) ) {
        return;
      }
      
      var nums = [];
      var divisions = 2000 / 10;
      var num = el.innerHTML;
      var isComma = /[0-9]+,[0-9]+/.test(num);
      num = num.replace(/,/g, '');
      var isRandomChar = num.match( /^(.*?)([0-9.][,.0-9]*[0-9]*)(.*?)$/ );
      num = isRandomChar ? isRandomChar[2] : num; 
      var isFloat = /^[0-9]+\.[0-9]+$/.test(num);
      var decimalPlaces = isFloat ? (num.split('.')[1] || []).length : 0;
      // Loop for
      for ( var val = divisions; val >= 1; val-- ) {
        var newNum = parseInt( num / divisions * val );
        // Check if value is float.
        if ( isFloat ) {
          newNum = parseFloat( num / divisions * val ).toFixed( decimalPlaces );
        }
        // Check if value has comma.
        if ( isComma ) {
          while (/(\d+)(\d{3})/.test(newNum.toString())) {
            newNum = newNum.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
          }
        }
        nums.unshift( newNum );
      }
      el.innerHTML = isRandomChar[1] + 0 + isRandomChar[3];
      // Function for displaying output with the set time and delay.
      var output = function() {
        el.innerHTML = isRandomChar[1] + nums.shift() + isRandomChar[3];
        if ( nums.length ) {
          setTimeout( output, 10 );
        }  
      }
      setTimeout( output, 10 );
    });
  }
}

var elements404 = document.querySelectorAll( '#countUp .number' );
if (elements404) {
  Array.prototype.forEach.call( elements404, function( el, i ) {

    // If no number, don't do anything.
    if ( ! /[0-9]/.test( el.innerHTML ) ) {
      return;
    }
    
    var nums = [];
    var divisions = 2000 / 10;
    var num = el.innerHTML;
    var isComma = /[0-9]+,[0-9]+/.test(num);
    num = num.replace(/,/g, '');
    var isRandomChar = num.match( /^(.*?)([0-9.][,.0-9]*[0-9]*)(.*?)$/ );
    num = isRandomChar ? isRandomChar[2] : num; 
    var isFloat = /^[0-9]+\.[0-9]+$/.test(num);
    var decimalPlaces = isFloat ? (num.split('.')[1] || []).length : 0;
    // Loop for
    for ( var val = divisions; val >= 1; val-- ) {
      var newNum = parseInt( num / divisions * val );
      // Check if value is float.
      if ( isFloat ) {
        newNum = parseFloat( num / divisions * val ).toFixed( decimalPlaces );
      }
      // Check if value has comma.
      if ( isComma ) {
        while (/(\d+)(\d{3})/.test(newNum.toString())) {
          newNum = newNum.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
        }
      }
      nums.unshift( newNum );
    }
    el.innerHTML = isRandomChar[1] + 0 + isRandomChar[3];
    // Function for displaying output with the set time and delay.
    var output = function() {
      el.innerHTML = isRandomChar[1] + nums.shift() + isRandomChar[3];
      if ( nums.length ) {
        setTimeout( output, 10 );
      }  
    }
    setTimeout( output, 10 );
  });
}